﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;

namespace SmartCart
{
	public class ItemDetailsStore
	{
		public ItemDetailsStore (string itemName, double itemPrice, ImageSource itemImage)
		{
			this.Name = itemName;
			this.ItemPrice = itemPrice;
			this.Image = itemImage;
		}

		public string Name { private set; get; }

		public double ItemPrice { private set; get; }

		public Color FavoriteColor { private set; get; }

		public ImageSource Image { private set; get; }

		public ImageSource BannerImage { private set; get; }

	};

}