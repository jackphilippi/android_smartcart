﻿using System;

using Xamarin.Forms;
using System.Collections.Generic;

namespace SmartCart
{
	public class ShoppingListItem
	{
		public ShoppingListItem (int itemID, string itemName, double itemPrice, ImageSource itemImage)
		{
			this.ID = itemID;
			this.Name = itemName;
			this.ItemPrice = itemPrice;
			this.Image = itemImage;
		}

		public int ID { private set; get; }

		public string Name { private set; get; }

		public double ItemPrice { private set; get; }

		public ImageSource Image { private set; get; }

	};

}