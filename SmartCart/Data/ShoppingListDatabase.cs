﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace SmartCart
{
	public class ShoppingListDatabase
	{
		static object locker = new object ();

		SQLiteConnection database;

		/// <summary>
		/// Initializes a new instance of the <see cref="Tasky.DL.TaskDatabase"/> TaskDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		public ShoppingListDatabase()
		{
			database = DependencyService.Get<ISQLite> ().GetConnection ();
			// create the tables
			database.CreateTable<ShoppingList>();
		}

		public IEnumerable<ShoppingList> GetItems ()
		{
			lock (locker) {
				return (from i in database.Table<ShoppingList>() select i).ToList();
			}
		}

		public ShoppingList GetItem (int id) 
		{
			lock (locker) {
				return database.Table<ShoppingList>().FirstOrDefault(x => x.ID == id);
			}
		}
	}
}

