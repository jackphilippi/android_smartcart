﻿using System;
using Xamarin.Forms;

namespace SmartCart
{
    public interface IBarcodeWriter
    {
		void SetImageText(string _imageText);
        Image GetImage();
    }
}
