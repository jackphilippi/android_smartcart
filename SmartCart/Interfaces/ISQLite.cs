﻿using System;
using SQLite;

namespace SmartCart
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

