﻿using System;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace SmartCart
{
	public interface IBarcodeScanner
	{
		Task OpenScanner();

		string Result();
	}
}
