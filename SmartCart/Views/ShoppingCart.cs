﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SQLiteNetExtensions.Attributes;
using Xamarin.Forms;
using SQLite;
using SmartCart;

namespace SmartCart
{
	class ShoppingCartView : ContentPage
	{
		public ListView listView;

		public ObservableCollection<ShoppingListItem> shoppingCart { get; set; }

		public StackLayout main;

		public Label lblHelp;

		public int _itemID;

		public double _itemPrice;

		public ShoppingCartView()
		{
			Title = "Shopping Cart";

			lblHelp = new Label ();
			lblHelp.Text = "Add items to your Shopping Cart below while you shop, then check out when you're done!";
			lblHelp.TextColor = Color.Black;
			lblHelp.FontAttributes = FontAttributes.Bold;

			Button btnCheckOut = new Button ();
			btnCheckOut.Text = "Check Out";

			btnCheckOut.Clicked += (object sender, EventArgs e) => {
				Navigation.PushAsync(new Payment());
			};

			shoppingCart = new ObservableCollection<ShoppingListItem>{};

			// Assign entries in DB to ShoppingList
			PopulateListView ();

			// Create the ListView.
			listView = new ListView
			{
				RowHeight = 80,
				// Source of data items.
				ItemsSource = shoppingCart,

				// Define template for displaying each item.
				// (Argument of DataTemplate constructor is called for 
				//      each item; it must return a Cell derivative.)
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.

						Label lblItemName = new Label();
						lblItemName.SetBinding(Label.TextProperty, "Name");
						lblItemName.TextColor = Color.Black;

						Label lblDollar = new Label();
						lblDollar.Text = "$";
						lblDollar.TextColor = Color.Black;

						Label lblItemPrice = new Label();
						lblItemPrice.SetBinding(Label.TextProperty, "ItemPrice".ToString());
						lblItemPrice.TextColor = Color.Black;

						Image imgDeleteButton = new Image();
						imgDeleteButton.Source = "delete.png";
						imgDeleteButton.WidthRequest = 50;
						imgDeleteButton.HeightRequest = 50;

						// As James Montemagno often says: "Ignore me, just doing something naughty..."
						// Work-around for grabbing the ID binding from 
						Label lblItemID = new Label ();
						lblItemID.SetBinding(Label.TextProperty, "ID".ToString());
						lblItemID.TextColor = Color.White;

						// Create tap event for ImageButton
						var scTapGestureRecognizer = new TapGestureRecognizer();
						scTapGestureRecognizer.Tapped += async (s, e) => {
							//TODO: Delete item
							var deldb = DependencyService.Get<ISQLite> ().GetConnection ();
							var deltable = await DisplayAlert("Delete Item", "Are you sure you want to delete the item from your cart?", "Yes", "No");
							if (deltable) {
								deldb.Query<ShoppingCartItem> ("DELETE FROM ShoppingCart WHERE itemID = ?", lblItemID.Text);
								PopulateListView();
								lblHelp.Text = "Your Shopping List has been updated!";
							}
						};			
						imgDeleteButton.GestureRecognizers.Add(scTapGestureRecognizer);// As James Montemagno often says: "Ignore me, just doing something naughty..."

						Image image = new Image();
						image.SetBinding(Image.SourceProperty, "Image");
						image.WidthRequest = 70;
						// Return an assembled ViewCell.

						return new ViewCell
						{
							View = new StackLayout
							{
								BackgroundColor = Color.FromHex("#FFFFFF"),
								Padding = new Thickness(0, 5),
								Orientation = StackOrientation.Horizontal,
								Children = 
								{
									// Placeholder for Item image
									image,
									new StackLayout
									{
										VerticalOptions = LayoutOptions.Center,
										Spacing = 0,
										Children = 
										{
											lblItemName,
											new StackLayout {
												Orientation = StackOrientation.Horizontal,
												Children = {
													lblDollar,
													lblItemPrice,
													lblItemID
												}
											}
										}
									},
									new StackLayout
									{
										HorizontalOptions = LayoutOptions.EndAndExpand,
										VerticalOptions = LayoutOptions.Center,
										Padding = new Thickness(0, 0, 15, 0),
										Children = 
										{
											imgDeleteButton
										}
									}
								}
							}
						};
					})
			};

			// Build the page.
			// Possibly change to scrollview?????

			main = new StackLayout
			{
				BackgroundColor = Color.FromHex("#FFFFFF"),
				Padding = new Thickness(10, 5),
				Children = 
				{
					new StackLayout 
					{
						BackgroundColor = Color.FromHex("#FDE8A1"),
						Padding = new Thickness(3),
						Children = {
							lblHelp
						}
					},
					listView,
					btnCheckOut
				}
				};
			this.Content = main;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			listView.ItemsSource = shoppingCart;
			PopulateListView ();
		}

		public void PopulateListView() {
			shoppingCart.Clear ();
			lblHelp.Text = "Add items to your Shopping Cart below while you shop, then check out when you're done!";
			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var SCtable = db.Query<ShoppingCartItem> ("SELECT * FROM ShoppingCart");
			foreach (var SCitem in SCtable) {
				var table = db.Query<ItemDetails> ("SELECT * FROM ItemDetails WHERE ID = ?", SCitem.itemID);
				foreach (var item in table) {
					var img = ImageSource.FromStream (() => new MemoryStream (item.Image));
					var itemi = new ShoppingListItem (item.ID, item.Name, item.ItemPrice, img);
					shoppingCart.Add (itemi);
				}
			}
		}
	}
}


