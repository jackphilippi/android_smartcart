﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;


namespace SmartCart
{
	public class SearchItem : ContentPage
	{
		SearchListView list;

		public SearchItem()
		{
			Title = "Search Item";
			list = new SearchListView ();

			SearchBar searchBar = new SearchBar {
				Placeholder = " Type your search query ",
			};

			searchBar.TextChanged += (sender, e) => list.FilterSearchList (searchBar.Text);
			searchBar.SearchButtonPressed += (sender, e) => {
				list.FilterSearchList (searchBar.Text);
			};

			var stack = new StackLayout () {
				Children = {
					searchBar,
					list
				}
			};

			Content = stack;

		}

	}

}


