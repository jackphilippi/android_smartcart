﻿/*
 * @author Jack Philippi
 * IAB330 Semester 2 2015
 * 
 * Allows scanning of barcodes and QR codes and re-routes user to 
 * individual item details page
 */

using System;
using Xamarin.Forms;
using SmartCart;
using System.Diagnostics;

namespace SmartCart 
	{
	public class ScanItem : ContentPage
	{
		public ScanItem ()
		{
			Title = "Scan Item";

			/* Placeholder for camera */
			Button btnScan = new Button ();
			btnScan.Text = "Scan Barcode";

			Image imgShutter = new Image ();
			imgShutter.Source = "shutter.png";
			imgShutter.WidthRequest = 110;

			Label lblHeader = new Label ();
			lblHeader.Text = "Scan A Barcode";
			lblHeader.FontAttributes = FontAttributes.Bold;
			lblHeader.FontSize = 30;
			lblHeader.TextColor = Color.Black;

			Label lblBody = new Label ();
			lblBody.Text = "Scan an item barcode whilst shopping to instantly view more details or add it to your cart!";
			lblBody.TextColor = Color.Black;
			lblBody.FontSize = 20;

			Image imgScannerExample = new Image ();
			imgScannerExample.Source = ImageSource.FromFile ("scannerexample.jpg");
			imgScannerExample.Aspect = Aspect.AspectFit;

			btnScan.Clicked += async (sender, e) => {
				await DependencyService.Get<IBarcodeScanner>().OpenScanner();
				string _result = DependencyService.Get<IBarcodeScanner>().Result ();
				if (_result != "") {

					var db = DependencyService.Get<ISQLite> ().GetConnection ();
					var table = db.Query<ItemDetails> ("SELECT ID, Name FROM ItemDetails WHERE ID = ?", _result);
					if (table.Count != 1) {
						DisplayAlert ("Error", "Scanned item ID '" + _result + "' doesn't exist in ItemStore. Please contact one of our employees for assistance.", "Continue");
					} else {
						foreach (var item in table) {
							var resultName = item.Name;
							var resultID = item.ID;
							ParseBarCode (resultID, resultName);
						}
					}
				}
			};

			StackLayout main = new StackLayout {
				BackgroundColor = Color.FromHex("#FFFFFF"),
				Children = {
					new StackLayout {
						Padding = new Thickness (10, 0, 10, 10),
						Children = {
							lblHeader,
							lblBody
						}
					},
					new StackLayout {
						Padding = new Thickness(20, 0),
						Children = {
							imgScannerExample
						}
					},
					new StackLayout {
						VerticalOptions = LayoutOptions.EndAndExpand,
						Children = {
							btnScan
						}
					}
				}
			};
			Content = main;
		}

		async void ParseBarCode (int _resultID, string _resultName) {
			var cont = await DisplayAlert ("Barcode Scanned", "Item '" + _resultName + "' was scanned. Would you like to view more details?", "Yes", "No");
			if (cont) {
				var db = DependencyService.Get<ISQLite> ().GetConnection ();
				var table = db.Query<ItemDetails> ("SELECT ID FROM ItemDetails WHERE ID = ?", _resultID);
				foreach (var item in table) {
					await Navigation.PushAsync (new ItemDetailsPage (item.ID));
				}
			}
		}
	}
}


