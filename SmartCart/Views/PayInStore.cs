﻿using System;
using Xamarin.Forms;

namespace SmartCart {
	
public class PayInStore : ContentPage
{
		public string shoppingCartContents;
		public Image barcode;

		public PayInStore () 
		{
			Title = "Pay In-store";

//			var deldb = DependencyService.Get<ISQLite> ().GetConnection ();
//			var table = deldb.Query<ShoppingCartItem> ("SELECT * FROM ShoppingCart");
//			foreach (var item in table) {
//				shoppingCartContents += "*" + item.ID;
//				System.Diagnostics.Debug.WriteLine (shoppingCartContents);
//			}

			//TODO: Update to show shopping cart contents
			DependencyService.Get<IBarcodeWriter> ().SetImageText ("No items are in your shoppingcart yet");
			barcode = DependencyService.Get<IBarcodeWriter> ().GetImage ();
			barcode.WidthRequest = 300;
			barcode.HeightRequest = 300;

			Label lblHelp = new Label ();
			lblHelp.Text = "Please show this to one of our " +
						   "friendly staff at a register!";
			lblHelp.HorizontalOptions = LayoutOptions.Start;
			lblHelp.TextColor = Color.Black;
			lblHelp.FontAttributes = FontAttributes.Bold;

			Button btnClearcart = new Button ();
			btnClearcart.HorizontalOptions = LayoutOptions.Fill;
			btnClearcart.Text = "Done!";
			btnClearcart.Clicked += async (object sender, EventArgs e) => {
				var cont = await DisplayAlert("Finished Shopping", "Are you sure you want to complete your shop? Clicking 'yes' will clear your shopping cart!", "Yes", "No");
				if (cont) {
					var cldb = DependencyService.Get<ISQLite> ().GetConnection ();
					cldb.Query<ShoppingList> ("DELETE FROM ShoppingCart");
				}
				await Navigation.PopAsync();
			};

			Label lblTotal = new Label ();
			lblTotal.Text = "Order Total: "+"$13.05"; 

			//TODO: ~~~ Display total (SELECT SUM(PRICE) FROM CART);
			lblTotal.FontAttributes = FontAttributes.Bold;
			lblTotal.TextColor = Color.Black;

			Label lblSavings = new Label ();
			lblSavings.Text = "You've Saved: "+"$0.75"; 
			//TODO: ~~~ Display savings (SELECT (oldprice - newprice) FROM items NATURAL JOIN cart ON items.id = cart.items_id;)
			lblSavings.TextColor = Color.Red;

			StackLayout main = new StackLayout {
				BackgroundColor = Color.FromHex("#FFFFFF"),
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness(20),
				Children = {
					new StackLayout() {
						VerticalOptions = LayoutOptions.StartAndExpand,
						Children = {
							barcode
						}
					},
					new StackLayout() {
						VerticalOptions = LayoutOptions.End,
						Children = {
							lblHelp
						}
					},
					new StackLayout() {
						VerticalOptions = LayoutOptions.End,
						Orientation = StackOrientation.Horizontal,
						Children = {
							new StackLayout() {
								HorizontalOptions = LayoutOptions.StartAndExpand,
								Children = {
									btnClearcart
								}
							},
							new StackLayout() {
								HorizontalOptions = LayoutOptions.End,
								Children = {
									lblTotal,
									lblSavings
								}
							}
						}
					}
				}
			};
		Content = main;
		}
	}
}

