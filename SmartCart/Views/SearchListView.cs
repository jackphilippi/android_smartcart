﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace SmartCart
{
	public class SearchListView : ListView
	{
		List<SearchItemInList> searchList = new List<SearchItemInList>{ };

		public class SearchItemInList
		{
			public SearchItemInList(int itemID, string itemName)
			{
				this.ID = itemID;
				this.Name = itemName;
			}

			public int ID { private set; get; }

			public string Name { private set; get; }
		
		};

		public SearchListView (){

			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var table = db.Table<ItemDetails> ();

			foreach (var item in table) {
				var itemi = new SearchItemInList (item.ID, item.Name);

				searchList.Add (itemi);

				var cell = new DataTemplate (typeof(TextCell));

				cell.SetBinding (TextCell.TextProperty, "Name");
				// Change the color of items text
				// cell.SetValue (TextCell.TextColorProperty, Color.Black);
				ItemTemplate = cell;

				ItemsSource = searchList;
			}

			ItemSelected += async (Object sender, SelectedItemChangedEventArgs e) => {
				if (SelectedItem == null)
					return;
				var selected = (SearchItemInList)e.SelectedItem;

				await Navigation.PushAsync(new ItemDetailsPage(selected.ID));
				SelectedItem = null;
			};
		}

		public void FilterSearchList (string filter)
		{
			this.BeginRefresh ();

			if (string.IsNullOrWhiteSpace (filter)) {
				this.ItemsSource = searchList;
			} else {
				this.ItemsSource = searchList
					.Where (x => x.Name.ToLower ()
						.Contains (filter.ToLower ()));
			}

			this.EndRefresh ();
		}
	};
}
