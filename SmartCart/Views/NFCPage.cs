﻿using NdefLibrary.Ndef;
using Poz1.NFCForms.Abstract;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace SmartCart
{
	public class NFCPage : ContentPage
	{
		private readonly INfcForms device;
		private StackLayout welcomePanel;

		private Switch IsWriteable;
		private Switch IsConnected;
		private Switch IsNDEFSupported;

		private ListView TechList;
		private ListView NDEFMessage;

		public NFCPage()
		{
			device = DependencyService.Get<INfcForms>();
			device.NewTag += HandleNewTag;
			device.TagConnected += device_TagConnected;
			device.TagDisconnected += device_TagDisconnected;

			Grid mainGrid = new Grid()
			{
				RowDefinitions = 
				{
					new RowDefinition { Height = new GridLength(10,GridUnitType.Star)},
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height =  new GridLength(5,GridUnitType.Star)}
				}
			};

			IsWriteable = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
			Label IsWriteableLabel = new Label() { Text = "Write Unlocked", HorizontalOptions = LayoutOptions.Center };

			IsConnected = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
			Label IsConnectedLabel = new Label() { Text = "Tag Connected", HorizontalOptions = LayoutOptions.Center };

			IsNDEFSupported = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
			Label IsNDEFSupportedLabel = new Label() { Text = "NDEF Support", HorizontalOptions = LayoutOptions.Center };

			NDEFMessage = new ListView();

			Button writeButton = new Button() { Text = "Write Tag" };
			writeButton.Clicked += HandleClicked;

			Label welcomeLabel = new Label
			{
				Text = "Scan an item!~~",
				XAlign = TextAlignment.Center,
				TextColor = Color.Black,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
			};

			welcomePanel = new StackLayout()
			{
				//TODO: Show 'a
				Children = { welcomeLabel },
				BackgroundColor = Color.White
			};

			mainGrid.Children.Add(NDEFMessage, 0, 1);
			mainGrid.Children.Add(writeButton, 0, 2);
			mainGrid.Children.Add(welcomePanel, 0, 1, 0, 4);

			Content = mainGrid;

		}

		void device_TagDisconnected(object sender, NfcFormsTag e)
		{
			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
			IsConnected.IsToggled = false;
			});
			#else
			IsConnected.IsToggled = false;
			#endif

		}

		void device_TagConnected(object sender, NfcFormsTag e)
		{

			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
			IsConnected.IsToggled = true;
			});
			#else
			IsConnected.IsToggled = true;
			#endif

		}

		void HandleClicked(object sender, EventArgs e)
		{
			var spRecord = new NdefSpRecord
			{
				NfcAction = NdefSpActRecord.NfcActionType.DoAction,
			};
			spRecord.AddTitle(new NdefTextRecord
				{
					Text = "00001:5.49"
				});
			// Add record to NDEF message
			var msg = new NdefMessage { spRecord };
			try
			{
				device.WriteTag(msg);
			}
			catch (Exception excp)
			{
				DisplayAlert("Error", excp.Message, "OK");
			}
		}

		private ObservableCollection<string> readNDEFMEssage(NdefMessage message)
		{

			ObservableCollection<string> collection = new ObservableCollection<string>();
			foreach (NdefRecord record in message)
			{
				// Go through each record, check if it's a Smart Poster
				if (record.CheckSpecializedType(false) == typeof(NdefSpRecord))
				{
					// Convert and extract Smart Poster info
					var spRecord = new NdefSpRecord(record);
					collection.Add("Text: " + spRecord.Titles[0].Text);
				}

				if (record.CheckSpecializedType(false) == typeof(NdefUriRecord))
				{
					// Convert and extract Smart Poster info
					var spRecord = new NdefUriRecord(record);
					collection.Add("Text: " + spRecord.Uri);
				}
			}
			return collection;
		}

		void HandleNewTag(object sender, NfcFormsTag e)
		{

			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{

			welcomePanel.IsVisible = false;

			IsWriteable.IsToggled = e.IsWriteable;
			IsNDEFSupported.IsToggled = e.IsNdefSupported;

			if (TechList != null)
			TechList.ItemsSource = e.TechList;

			if (e.IsNdefSupported)
			NDEFMessage.ItemsSource = readNDEFMEssage(e.NdefMessage);
			});
			#else
			welcomePanel.IsVisible = false;

			IsWriteable.IsToggled = e.IsWriteable;
			IsNDEFSupported.IsToggled = e.IsNdefSupported;

			if(TechList != null)
				TechList.ItemsSource = e.TechList;

			if (e.IsNdefSupported)
				NDEFMessage.ItemsSource = readNDEFMEssage(e.NdefMessage);
			#endif
		}
	}
}