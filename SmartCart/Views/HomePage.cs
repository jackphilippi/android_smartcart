﻿/*
 * REFERENCE LINKS
 * 
 * PushAsync / ModalAsync .etc
 * https://forums.xamarin.com/discussion/24445/push-async-and-pop-async-in-xamarin-forms
 */

using System;

using Xamarin.Forms;

namespace SmartCart
{
	public class HomePage : ContentPage
	{
		public HomePage ()
		{
			Title = "Home";
			Icon = "icon.png";

			// Create a 1px-high BoxView for Separator after logo
			BoxView separator = new BoxView {
				BackgroundColor = Color.FromHex("#9a9eb9")
			};
			separator.HeightRequest = 1;
			separator.WidthRequest = 360;
			separator.HorizontalOptions = LayoutOptions.Center;

			Button btnAbout = new Button ();
			btnAbout.Text = "About Us";
			btnAbout.BackgroundColor = Color.FromHex("#444444");
			btnAbout.BorderWidth = 1;
			btnAbout.Clicked += async (sender, e) => {
				await DisplayAlert("About Us", "This application was created as an assessment for part of the IAB330 unit at QUT by Jack Philippi, Gurwinder Singh Chahal and Joongsan Jun.", "Thanks for having a look!");
			};

			// Define images for main page navigation
			var smartCartImg = new Image ();
			smartCartImg.Source = "smartcartimg.png";
			smartCartImg.HorizontalOptions = LayoutOptions.Start;
			smartCartImg.WidthRequest = 180;

			var oldSchoolImg = new Image ();
			oldSchoolImg.Source = "oldschoolimg.png";
			oldSchoolImg.HorizontalOptions = LayoutOptions.End;
			oldSchoolImg.WidthRequest = 180;

			// Create tap event for linkable images
			var scTapGestureRecognizer = new TapGestureRecognizer();
			scTapGestureRecognizer.Tapped += (s, e) => {
				Navigation.PushAsync (new PairCart ());
			};
			var osTapGestureRecognizer = new TapGestureRecognizer();
			osTapGestureRecognizer.Tapped += (s, e) => {
				Navigation.PushAsync (new HomeTabs ());
			};
			// Link gestures to Images
			smartCartImg.GestureRecognizers.Add(scTapGestureRecognizer);
			oldSchoolImg.GestureRecognizers.Add(osTapGestureRecognizer);

			// Define logo as a new image using logo.png
			var logo = new Image ();
			logo.Source = "logo.png";

			// Create the 'wrapper' StackLayout which will be the parent for other layouts
			StackLayout main = new StackLayout {
				VerticalOptions = LayoutOptions.FillAndExpand,
				Spacing = 0,
				Children = {
					// Layout to hold logo
					new StackLayout {
						BackgroundColor = Color.FromHex("#FFFFFF"),
						Padding = new Thickness (30, 10, 30, 10),
						VerticalOptions = LayoutOptions.Start,
						// Layout has one child which is the logo image
						Children = {
							logo
						}
					},
					// Layout to hold separator and content buttons
					new StackLayout {
						VerticalOptions = LayoutOptions.FillAndExpand,
						Padding = new Thickness(0, 0, 0, 10),
						BackgroundColor = Color.FromHex ("#E4E4E4"),
						Children = {
							separator,
							smartCartImg,
							oldSchoolImg
						}
					},
					new StackLayout {
						VerticalOptions = LayoutOptions.End,
						Children = {
							btnAbout
						}
					}
				}
			};
			Content = main;
		}
	}
};
