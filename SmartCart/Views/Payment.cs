﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace SmartCart
{
	class Payment : ContentPage
	{
		class PaymentContent
		{
			public PaymentContent(string paymentName, string paymentImage)
			{
				this.PaymentName = paymentName;
				this.PaymentImage = paymentImage;
			}

			public string PaymentName { private set; get; }

			public string PaymentImage { private set; get; }
		};

		public Payment()
		{
			Title = "Payment Option";

			List<PaymentContent> methods = new List<PaymentContent>
			{
				new PaymentContent("PayPal", "paypal.jpg"),
				new PaymentContent("MasterCard", "mastercard.png"),
				new PaymentContent("BitCoin", "bitcoin.jpg"),
				new PaymentContent("Instore", "instore.jpg"),
				new PaymentContent("Other Options...", "")
			};

			ListView listView = new ListView
			{
				RowHeight = 80,
				ItemsSource = methods,
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.
						Label PaymentName = new Label();
						PaymentName.SetBinding(Label.TextProperty, "PaymentName");
						PaymentName.TextColor = Color.Black;
						PaymentName.FontSize = 20;
						PaymentName.FontAttributes = FontAttributes.Bold;

						Image image = new Image();
						image.SetBinding(Image.SourceProperty, "PaymentImage");
						image.WidthRequest = 70;

						return new ViewCell
						{
							View = new StackLayout
							{
								BackgroundColor = Color.FromHex("#FFFFFF"),
								Padding = new Thickness(0, 5),
								Orientation = StackOrientation.Horizontal,
								Children = 
								{
									// Placeholder for Item image
									image,
									new StackLayout
									{
										VerticalOptions = LayoutOptions.Center,
										Spacing = 0,
										Padding = new Thickness(50,5),
										Children =
										{
											PaymentName
										}
									},
								}
							}
						};
					})
			};

			listView.ItemSelected += async (Object sender, SelectedItemChangedEventArgs e) => {
				listView.SelectedItem = null;

				// For selections that aren't payInstore, throw error msg
				if ((PaymentContent)e.SelectedItem == methods[0]) {
					await DisplayAlert("Disabled", "This feature is not available in the current release", "Okay");
				} else if ((PaymentContent)e.SelectedItem == methods[1]) {
					await DisplayAlert("Disabled", "This feature is not available in the current release", "Okay");
				} else if ((PaymentContent)e.SelectedItem == methods[2]) {
					await DisplayAlert("Disabled", "This feature is not available in the current release", "Okay");
				} else if ((PaymentContent)e.SelectedItem == methods[3]) {
					await Navigation.PushAsync (new PayInStore ());
				} else if ((PaymentContent)e.SelectedItem == methods[4]) {
					await DisplayAlert("Disabled", "This feature is not available in the current release", "Okay");
				}
			};

			// Build the page.
			this.Content = new StackLayout
			{
				BackgroundColor = Color.FromHex("#FFFFFF"),
				Padding = new Thickness(10, 5),
				Children = 
				{
					listView
				}
			};
		}
	}
}