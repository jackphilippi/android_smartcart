﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using SQLitePCL;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace SmartCart
{
	public class ItemDetailsPage : ContentPage
	{
		public int _itemID;
		public string _itemName;
		public double _itemPrice;
		public double _itemDiscountedPrice;
		public string _itemDescription;
		public ImageSource _itemBannerImage;

		public ItemDetailsPage(int itemID)
		{
			
			Title = "Item Details";

			_itemID = itemID;

			// Get item information from database
			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var table = db.Query<ItemDetails> ("SELECT * FROM ItemDetails WHERE ID = ?", itemID);
			foreach (var item in table) {
				_itemName = item.Name;
				_itemPrice = item.ItemPrice;
				_itemDiscountedPrice = item.DiscountedItemPrice;
				_itemBannerImage = ImageSource.FromStream (() => new MemoryStream (item.BannerImage));
				//TODO: Add Description
			}
				
			Label lblItemName = new Label { 
				Text = _itemName,
				TextColor = Color.Black,
				FontSize = 30
			};

			Image itemBannerImage = new Image ();
			itemBannerImage.SetValue (Image.SourceProperty, _itemBannerImage);
			itemBannerImage.HorizontalOptions = LayoutOptions.FillAndExpand;

			BoxView separator1 = new BoxView {
				BackgroundColor = Color.FromHex("#555555"),
				HeightRequest = 1,
				WidthRequest = 360,
				HorizontalOptions = LayoutOptions.Center
			};
			BoxView separator2 = new BoxView {
				BackgroundColor = Color.FromHex("#999999"),
				HeightRequest = 1,
				WidthRequest = 360,
				HorizontalOptions = LayoutOptions.Center
			};
			BoxView separator3 = new BoxView {
				BackgroundColor = Color.FromHex("#B6B6B6"),
				HeightRequest = 1,
				WidthRequest = 360,
				HorizontalOptions = LayoutOptions.Center
			};

			Image itemImage = new Image();
			itemImage.SetBinding(Image.SourceProperty, "Image");
			itemImage.WidthRequest = 70;

			Image imgAddToCart = new Image();
			imgAddToCart.Source = "addtocart.png";
			imgAddToCart.WidthRequest = 50;
			imgAddToCart.HeightRequest = 50;

			Image imgAddToShoppingList = new Image();
			imgAddToShoppingList.Source = "addtoshoppinglist.png";
			imgAddToShoppingList.WidthRequest = 50;
			imgAddToShoppingList.HeightRequest = 50;

			Label lblPrice = new Label { 
				Text = "Original Price: " + _itemPrice.ToString("C2"),
				TextColor = Color.Black
			};

			Label lblDiscountedPrice = new Label { 
				Text = "Current Price: " + _itemDiscountedPrice.ToString("C2"), 
				FontAttributes = FontAttributes.Bold,
				TextColor = Color.Black
			};

			double _ItemPriceDifference = _itemPrice - _itemDiscountedPrice;

			Label lblPriceDifference = new Label {
				Text = "You Save: " + _ItemPriceDifference.ToString("C")
			};

			Label lblItemDescription = new Label();
			lblItemDescription.SetBinding(Label.TextProperty, "ItemDescription");
			lblItemDescription.Text = "This is a quality product from the finest ranges in Australia. Only in the lush outback can such luxurious products thrive, and this" + 
				" is why we take pride in our product. Our product has zero preservatives, no added colours or flavours, and is great for kids";
			lblItemDescription.TextColor = Color.Black;
			lblItemDescription.FontSize = 16;

			if (_itemPrice.CompareTo (_itemDiscountedPrice) <= 0) {
				lblPriceDifference.TextColor = Color.FromHex ("#E4E4E4");
				lblPrice.TextColor = Color.FromHex ("#E4E4E4");
			} else {
				lblPriceDifference.TextColor = Color.Blue;
			}

			// Create tap event for ImageButton
			var atcTapGestureRecognizer = new TapGestureRecognizer();
			atcTapGestureRecognizer.Tapped += async (s, e) => {
				//Add to cart table in db
				var res = await DisplayAlert("Add to cart?", "Do you want to add the item '" + _itemName + "' to your cart?", "Yes", "No");
				if (res) {
					InsertIntoShoppingCart();//execute query -> add to cart
					await DisplayAlert ("Added To Cart", "'" + _itemName + "' was added to your cart", "Continue Shopping");
				}
			};			
			imgAddToCart.GestureRecognizers.Add(atcTapGestureRecognizer);

			// Create tap event for ImageButton
			var atslTapGestureRecognizer = new TapGestureRecognizer();
			atslTapGestureRecognizer.Tapped += async (s, e) => {
				//Add to cart table in db
				var res = await DisplayAlert("Add to Shopping List?", "Do you want to add the item '" + _itemName + "' to your Shopping List?", "Yes", "No");
				if (res) {
					//execute query -> add to shopping list
					InsertIntoShoppingList();
					await DisplayAlert ("Added To Shopping List", "'" + _itemName + "' was added to your Shopping List", "Continue Browsing");
				}

			};			
			imgAddToShoppingList.GestureRecognizers.Add(atslTapGestureRecognizer);

			Content = new StackLayout {
				BackgroundColor = Color.FromHex ("#E4E4E4"),
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					new StackLayout {
						HorizontalOptions = LayoutOptions.FillAndExpand,
						Children = {
							new StackLayout {
								Spacing = 0,
								HorizontalOptions = LayoutOptions.StartAndExpand,
								Children = {
									itemBannerImage,
									separator1,
									separator2,
									separator3
								}
							}
						}
					},
					new StackLayout {
						Padding = new Thickness(20, 0, 20, 20),
						Orientation = StackOrientation.Horizontal,
						Children = {
							new StackLayout {
								HorizontalOptions = LayoutOptions.StartAndExpand,
								Children = {
									lblItemName,
									lblDiscountedPrice,
									lblPrice,
									lblPriceDifference
								}
							},
							new StackLayout {
								Padding = new Thickness(10, 20, 0, 0),
								Orientation = StackOrientation.Vertical,
								HorizontalOptions = LayoutOptions.End,
								Children = {
									imgAddToCart,
									imgAddToShoppingList
								}
							}
						}
					},
					new StackLayout {
						Orientation = StackOrientation.Vertical,
						Padding = new Thickness(20, 0),
						Children = {
							lblItemDescription
						}
					}
				}
			};
		}

		public void InsertIntoShoppingList() {
			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var table = db.Execute("INSERT INTO ShoppingList(itemID) VALUES (?)", _itemID);
		}

		public void InsertIntoShoppingCart() {
			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var table = db.Execute("INSERT INTO ShoppingCart(itemID) VALUES (?)", _itemID);
		}
	}
}