﻿using System;
using System.IO;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SQLiteNetExtensions.Attributes;
using Xamarin.Forms;
using SQLite;
using SmartCart;

namespace SmartCart
{
	class ShoppingListView : ContentPage
	{
		public ListView listView;

		public ObservableCollection<ShoppingListItem> shoppingList { get; set; }

		public StackLayout main;

		public Label lblHelp;

		public ShoppingListView()
		{
			Title = "Shopping List";

			lblHelp = new Label ();
			lblHelp.Text = "Add items to your Shopping List below so that you don't forget them on your shop!";
			lblHelp.TextColor = Color.Black;
			lblHelp.FontAttributes = FontAttributes.Bold;

			Button btnClearList = new Button ();
			btnClearList.Text = "Clear Shopping List";

			btnClearList.Clicked += async (sender, e) => {
				var cldb = DependencyService.Get<ISQLite> ().GetConnection ();
				var cleartable = await DisplayAlert("Clear Shopping List", "Are you sure you want to clear your shopping list?", "Yes", "No");
				if (cleartable) {
					cldb.Query<ShoppingList> ("DELETE FROM ShoppingList");
					PopulateListView();
					shoppingList.Clear();
					lblHelp.Text = "Your Shopping List has been updated!";
				}
			};

			shoppingList = new ObservableCollection<ShoppingListItem>{};

			// Assign entries in DB to ShoppingList
			PopulateListView ();

			// Create the ListView.
			listView = new ListView
			{
				RowHeight = 80,
				// Source of data items.
				ItemsSource = shoppingList,

				// Define template for displaying each item.
				// (Argument of DataTemplate constructor is called for 
				//      each item; it must return a Cell derivative.)
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.

						Label lblItemName = new Label();
						lblItemName.SetBinding(Label.TextProperty, "Name");
						lblItemName.TextColor = Color.Black;

						Label lblDollar = new Label();
						lblDollar.Text = "$";
						lblDollar.TextColor = Color.Black;

						Label lblItemPrice = new Label();
						lblItemPrice.SetBinding(Label.TextProperty, "ItemPrice".ToString());
						lblItemPrice.TextColor = Color.Black; 

						Image imgDelete = new Image();
						imgDelete.Source = "delete.png";
						imgDelete.WidthRequest = 50;
						imgDelete.HeightRequest = 50;

						Label lblItemID = new Label ();
						lblItemID.SetBinding(Label.TextProperty, "ID".ToString());
						lblItemID.TextColor = Color.White;

						// Create tap event for ImageButton
						var scTapGestureRecognizer = new TapGestureRecognizer();
						scTapGestureRecognizer.Tapped += async (s, e) => {
							var deldb = DependencyService.Get<ISQLite> ().GetConnection ();
							var deltable = await DisplayAlert("Delete Item", "Are you sure you want to delete the item from your shopping list?", "Yes", "No");
							if (deltable) {
								deldb.Query<ShoppingList> ("DELETE FROM ShoppingList WHERE itemID = ?", lblItemID.Text);
								PopulateListView();
								lblHelp.Text = "Your Shopping List has been updated!";
							}
						};			
						imgDelete.GestureRecognizers.Add(scTapGestureRecognizer);

						Image image = new Image();
						image.SetBinding(Image.SourceProperty, "Image");
						image.WidthRequest = 70;
						// Return an assembled ViewCell.

						return new ViewCell
						{
							View = new StackLayout
							{
								BackgroundColor = Color.FromHex("#FFFFFF"),
								Padding = new Thickness(0, 5),
								Orientation = StackOrientation.Horizontal,
								Children = 
								{
									// Placeholder for Item image
									image,
									new StackLayout
									{
										VerticalOptions = LayoutOptions.Center,
										Spacing = 0,
										Children = 
										{
											lblItemName,
											new StackLayout {
												Orientation = StackOrientation.Horizontal,
												Children = {
													lblDollar,
													lblItemPrice,
													lblItemID
												}
											}
										}
									},
									new StackLayout
									{
										HorizontalOptions = LayoutOptions.EndAndExpand,
										VerticalOptions = LayoutOptions.Center,
										Padding = new Thickness(0, 0, 15, 0),
										Children = 
										{
											imgDelete
										}
									}
								}
							}
						};
				})
			};

			// Build the page

			main = new StackLayout
			{
				BackgroundColor = Color.FromHex("#FFFFFF"),
				Padding = new Thickness(10, 5),
				Children = 
				{
					new StackLayout 
					{
						BackgroundColor = Color.FromHex("#FDE8A1"),
						Padding = new Thickness(3),
						Children = {
							lblHelp
						}
					},
					listView,
					btnClearList
				}
			};
			this.Content = main;
		}

		protected override void OnAppearing ()
		{
			base.OnAppearing ();
			listView.ItemsSource = shoppingList;
			PopulateListView ();
		}

		public void PopulateListView() {
			shoppingList.Clear ();
			lblHelp.Text = "Add items to your Shopping List below so that you don't forget them on your shop!";
			var db = DependencyService.Get<ISQLite> ().GetConnection ();
			var SLtable = db.Query<ShoppingList> ("SELECT * FROM ShoppingList");
			foreach (var SLitem in SLtable) {
				var table = db.Query<ItemDetails> ("SELECT * FROM ItemDetails WHERE ID = ?", SLitem.itemID);
				foreach (var item in table) {
					var img = ImageSource.FromStream (() => new MemoryStream (item.Image));
					var itemi = new ShoppingListItem (item.ID, item.Name, item.ItemPrice, img);
					shoppingList.Add (itemi);
				}
			}
		}
	}
}