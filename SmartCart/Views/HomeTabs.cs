﻿/* View for tabbed controller
 * @description Initialises multiple ContentPage objects and adds them to the tabbed controller.
 * Activates the NFC controller and listens for NDEF tags (calling readNDEFMessage() when tag is read).
 */

using System;
using SmartCart;
using Xamarin.Forms;
using NdefLibrary.Ndef;
using Poz1.NFCForms.Abstract;
using System.Collections.ObjectModel;
using SQLite;

namespace SmartCart
{
	public class HomeTabs : TabbedPage
	{
		private readonly INfcForms device;

		private Switch IsWriteable;
		private Switch IsConnected;
		private Switch IsNDEFSupported;

		private ListView TechList;
		private ListView NDEFMessage;

		readonly Page searchTab;

		public HomeTabs () 
		{
			searchTab = new SearchItem();
			this.Children.Add (new ShoppingListView ());
			this.Children.Add (new ScanItem ());
			this.Children.Add (searchTab);
			this.Children.Add (new ShoppingCartView ());

			// Listen passively for tags
			device = DependencyService.Get<INfcForms>();
			device.NewTag += HandleNewTag;
			device.TagConnected += device_TagConnected;
			device.TagDisconnected += device_TagDisconnected;

			// Invisible switches for NFC feature
			IsWriteable = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
			IsConnected = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
			IsNDEFSupported = new Switch() { HorizontalOptions = LayoutOptions.Center, IsEnabled = false };
		}
				
		void device_TagDisconnected(object sender, NfcFormsTag e)
		{
			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
			IsConnected.IsToggled = false;
			});
			#else
			IsConnected.IsToggled = false;
			#endif

		}

		void device_TagConnected(object sender, NfcFormsTag e)
		{

			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
			IsConnected.IsToggled = true;
			});
			#else
			IsConnected.IsToggled = true;
			#endif

		}

		private ObservableCollection<string> readNDEFMEssage(NdefMessage message)
		{

			ObservableCollection<string> collection = new ObservableCollection<string>();
			foreach (NdefRecord record in message)
			{
				// Go through each record, check if it's a Smart Poster
				if (record.CheckSpecializedType(false) == typeof(NdefSpRecord))
				{
					// Convert and extract Smart Poster info
					var spRecord = new NdefSpRecord(record);
					var result = spRecord.Titles [0].Text;

					var db = DependencyService.Get<ISQLite> ().GetConnection ();
					var table = db.Query<ItemDetails> ("SELECT ID, Name FROM ItemDetails WHERE ID = ?", result);

					if (table.Count != 1) {
						DisplayAlert ("Error", "Scanned item ID '" + result + "' doesn't exist in ItemStore. Please contact one of our employees for assistance.", "Continue");
					} else {
						string resultName = table [0].Name;
						int resultID = table [0].ID;
						ParseCode (resultID, resultName);
					}
				}
			}
			return collection;
		}

		async void ParseCode (int _resultID, string _resultName) {
			var cont = await DisplayAlert ("NFC Item Scanned", "Item '" + _resultName + "' was scanned via NFC. View more details?", "Yes", "No");
			if (cont) {
				var db = DependencyService.Get<ISQLite> ().GetConnection ();
				var table = db.Query<ItemDetails> ("SELECT ID FROM ItemDetails WHERE ID = ?", _resultID);
				if (table.Count != 1) {
				} else {
					await Navigation.PushAsync (new ItemDetailsPage (table[0].ID));
				}
			}
		}

		void HandleNewTag(object sender, NfcFormsTag e)
		{
			
			#if SILVERLIGHT
			System.Windows.Deployment.Current.Dispatcher.BeginInvoke(() =>
			{

//			welcomePanel.IsVisible = false;

			IsWriteable.IsToggled = e.IsWriteable;
			IsNDEFSupported.IsToggled = e.IsNdefSupported;

			if (TechList != null)
			TechList.ItemsSource = e.TechList;

			if (e.IsNdefSupported)
			NDEFMessage.ItemsSource = readNDEFMEssage(e.NdefMessage);
			});
			#else
//			welcomePanel.IsVisible = false;

			IsWriteable.IsToggled = e.IsWriteable;
			IsNDEFSupported.IsToggled = e.IsNdefSupported;

			if(TechList != null)
				TechList.ItemsSource = e.TechList;

			if (e.IsNdefSupported)
				readNDEFMEssage(e.NdefMessage);
			#endif
		}
	}
}