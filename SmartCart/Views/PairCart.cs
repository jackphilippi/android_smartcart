﻿using System;

using Xamarin.Forms;

namespace SmartCart
{
	public class PairCart : ContentPage
	{
		public PairCart ()
		{
			
			Title = "Pair SmartCart";
			BackgroundColor = Color.FromHex("#E4E4E4");

			var titleStyling = new Style (typeof(Label)) {
				Setters = {
					new Setter {Property = Label.TextColorProperty, Value = Color.FromHex("#444444")}
					// new Setter {Property = Label.BackgroundColorProperty, Value = Color.Yellow}
				}
			};

			Label pairTitle = new Label ();
			pairTitle.Text = "Enter your SmartCart ID:";
			pairTitle.FontSize = 30;
			pairTitle.TextColor = Color.Black;
			pairTitle.HorizontalOptions = LayoutOptions.CenterAndExpand;

			Entry codeInput = new InputCodeEntry (new CustomEntryParams { MaxLength = 4 });
			codeInput.BackgroundColor = Color.FromHex ("FFFFFF");
			codeInput.TextColor = Color.FromHex ("444444");
			codeInput.WidthRequest = 200;
			codeInput.HeightRequest = 100;
			codeInput.HorizontalOptions = LayoutOptions.Center;
			codeInput.Placeholder = "Pair Code";

			Button pairButton = new Button ();
			pairButton.Text = "Pair!";
			codeInput.HorizontalOptions = LayoutOptions.Center;
			pairButton.Clicked += (sender, e) => {
//				Navigation.PushAsync (new HomeTabs ());
				DisplayAlert("Disabled", "This feature is not available in the current release", "Okay");
			};

			Label helpTitle = new Label ();
			helpTitle.Text = "Need Help?";
			helpTitle.Style = titleStyling;
			helpTitle.FontAttributes = FontAttributes.Bold;
			helpTitle.XAlign = TextAlignment.Center;
			helpTitle.FontSize = 20;

			Label helpText = new Label ();
			helpText.Style = titleStyling;
			helpText.Text = "You can find your pair code on your SmartCart's screen! " +
							"Once paired, items added to your physical cart appear " +
							"in your in-app shopping cart!";
			helpText.VerticalOptions = LayoutOptions.Center;
			helpText.XAlign = TextAlignment.Center;

			Image cartScreen = new Image ();
			cartScreen.Source = "cartscreen.png";
			cartScreen.WidthRequest = 300;

			StackLayout main = new StackLayout {
				Spacing = 0,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Padding = new Thickness (10),
				Children = {
					new StackLayout() {
						VerticalOptions = LayoutOptions.StartAndExpand,
						HorizontalOptions = LayoutOptions.Center,
						Children = {
							new StackLayout() {
								Padding = new Thickness(0, 0, 0, 10),
								Children = {
									pairTitle
								}
							},
							codeInput,
							pairButton
						}
					},
					new StackLayout() {
						VerticalOptions = LayoutOptions.FillAndExpand,
						Padding = new Thickness(0, 0, 0, 10),
						Children = {
							new StackLayout() {
								Children = {
									new StackLayout() {
										Padding = new Thickness (0, 0, 0, 10),
										Children = {
											helpTitle,
											helpText
										}
									},
									cartScreen
								}
							}
						}	
					}
				}
			};
			Content = main;
		}
	}
}

