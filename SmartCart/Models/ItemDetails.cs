﻿using System;
using SQLite;

using Xamarin.Forms;

namespace SmartCart
{
	public class ItemDetails : ContentPage  
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public double ItemPrice { get; set; }
		public double DiscountedItemPrice { get; set; }
		public byte[] Image { get; set; }
		public byte[] BannerImage { get; set; }
	}
}