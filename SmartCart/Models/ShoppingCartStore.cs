﻿using System;
using SQLite;
using Xamarin.Forms;
using SQLiteNetExtensions.Attributes;

namespace SmartCart
{
	public class ShoppingCartItem : ContentPage
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		[ForeignKey(typeof(ItemDetails))]
		public int itemID { get; set; }
	}
}