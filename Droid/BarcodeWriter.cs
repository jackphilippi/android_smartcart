﻿using System.IO;
using Xamarin.Forms;
using System;
using Android.Graphics;
using SmartCart;
using Android.Widget;

[assembly: Xamarin.Forms.Dependency (typeof (BarcodeGenerator))]

namespace SmartCart
{		
	public class BarcodeGenerator : IBarcodeWriter
	{
		public Image image = new Image();

		public void SetImageText(string _imageText) {
			image.Source = ImageSource.FromStream(() => 
				{
					var barcodeWriter = new ZXing.Mobile.BarcodeWriter {
						Format = ZXing.BarcodeFormat.QR_CODE,
						Options = new ZXing.Common.EncodingOptions {
							Width = 300,
							Height = 300
						}
					};
					var barcode = barcodeWriter.Write (_imageText);

					MemoryStream ms = new MemoryStream ();
					barcode.Compress (Bitmap.CompressFormat.Jpeg, 100, ms);
					ms.Position = 0;
					return ms;

				});
		}

		public Image GetImage() {
			return image;
		}
	}
}