﻿using System;
using SQLite;

namespace SmartCart.Droid
{
	public class CreateDB
	{
		public CreateDB ()
		{
			// Create our connection
			string folder = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var db = new SQLiteConnection (System.IO.Path.Combine (folder, "notes.db"));
			db.CreateTable<Item>();
		}
	}
}

