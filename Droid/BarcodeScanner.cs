﻿using System;
using SmartCart;
using System.Threading.Tasks;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency (typeof (BarcodeScannerAndroid))]

namespace SmartCart
{	
	public class BarcodeScannerAndroid : IBarcodeScanner
	{
		string resultText;

		public async Task OpenScanner() {
			var scanner = new ZXing.Mobile.MobileBarcodeScanner();
			resultText = String.Empty;
			await scanner.Scan().ContinueWith(t =>
				{
					if (t.Result != null)
						resultText = t.Result.Text;
				}); 
		}

		public string Result () {
			return resultText; 
		}
	}
}

